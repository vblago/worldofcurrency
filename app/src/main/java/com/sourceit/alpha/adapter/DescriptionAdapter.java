package com.sourceit.alpha.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.sourceit.alpha.R;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;

public class DescriptionAdapter extends RecyclerView.Adapter<DescriptionAdapter.ViewHolder> {

    public List<String> list;
    private Map<String, Float> map;
    private Context context;

    public DescriptionAdapter(Map<String, Float> map, Context context) {
        this.map = map;
        this.context = context;
        list = new ArrayList<>();
        for (Map.Entry<String, Float> entry : map.entrySet()) {
            list.add(entry.getKey());
        }
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(context).inflate(R.layout.item_description_layout, parent, false);
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        holder.bind(list.get(position));
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.name_currency)
        TextView nameView;
        @BindView(R.id.rate_currency)
        TextView rateView;

        ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

        void bind(String name) {
            nameView.setText(name);
            rateView.setText(String.valueOf(map.get(name)));
        }
    }
}

