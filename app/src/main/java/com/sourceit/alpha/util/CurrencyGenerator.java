package com.sourceit.alpha.util;


import com.sourceit.alpha.R;
import com.sourceit.alpha.model.Currency;

import java.util.ArrayList;
import java.util.List;

public class CurrencyGenerator {

    public static List<Currency> list(){
        List<Currency> currencyList = new ArrayList<>();

        currencyList.add(new Currency("USD", "The United States dollar", R.string.usd_desc, R.drawable.usd_img));
        currencyList.add(new Currency("EUR", "Euro", R.string.eur_desc, R.drawable.eur_img));
        currencyList.add(new Currency("RUB", "Russian ruble", R.string.rur_desc, R.drawable.rub_img));
        currencyList.add(new Currency("JPY", "Japanese yen", R.string.jpy_desc, R.drawable.jpy_img));
        currencyList.add(new Currency("GBP", "British pound", R.string.gbp_desc, R.drawable.gbp_img));

        return currencyList;
    }

}
