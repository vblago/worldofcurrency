package com.sourceit.alpha;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.sourceit.alpha.fragment.ListFragment;
import com.sourceit.alpha.fragment.SearchFragment;

public class MainActivity extends AppCompatActivity{

    ListFragment listFragment;
    SearchFragment searchFragment;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        listFragment = new ListFragment();
        searchFragment = new SearchFragment();

        getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.activity_main, listFragment)
                .commit();

    }

}
